# Listagem de repositórios do gitHub via Api

Listagem de todos os repositórios do Symfony hospedados no gitHub via Api 2.0


### Instalação

Clone o repositório no bitbucket

```
git clone https://jcarvalho1989@bitbucket.org/jcarvalho1989/teste-printi.git
```




## Para listar os repositórios

Basta acessar o arquivo index.php na pasta raiz do projeto e clicar no botão para iniciar a listagem.




## Feito com
* [PHP](https://secure.php.net/) - The main language
* [PHP GitHub API 2.0](https://github.com/KnpLabs/php-github-api/) - A simple Object Oriented wrapper for GitHub API
* [Composer](https://getcomposer.org/) - Dependency Management
* [Bootstrap](http://getbootstrap.com/) - The most popular HTML, CSS, and JS library in the world.


