$(document).ready(function () {
    $('#get_repo').on('click', function () {
        lista_repositorios = $('.repositorios');
        lista_repositorios.html('');
        lista_repositorios.addClass('loading');

        $.ajax({
            url: 'src/include/load_repositories.php',
            contentType: "text/html",
            type: "POST",
            success: function (result) {
                lista_repositorios.html(result);
                lista_repositorios.removeClass('loading');
            }

        });


    });

});

