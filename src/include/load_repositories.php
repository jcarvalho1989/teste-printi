<?php
header('Content-Type:  text/html');
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    require_once '../../vendor/autoload.php';
    require '../../src/Repo/Repositorio.php';

    $result = new Repositorio();
    $repositorios = (array) $result;
    ?>
    <div class="panel panel-default">
        <?php foreach ($repositorios as $repo) { ?>

            <div class="panel-heading">Total de repositórios encontrados <span class="badge"><?php echo count($repo); ?></span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <?php foreach ($repo as $key => $value) {
                        ?>
                        <div class="col-lg-6 repositorios-itens">
                            
                            <h4><span class="badge">#<?php echo $key + 1 ?></span>
                                <a href="<?php echo $value['html_url']; ?>" target="blanck"><?php echo $value['name'] ?></a></h4>
                            <p><?php echo (isset($value['description']) && !empty($value['description'])) ? $value['description'] : '&nbsp;' ?></p>
                            <div class="infos">
                                <div class="item-info">
                                    <span title="Language" class="glyphicon glyphicon-record" aria-hidden="true"></span><?php echo $value['language'] ?>
                                </div>
                                <div class="item-info">
                                    <span title="Forks"  class="glyphicon glyphicon-random" aria-hidden="true"></span><?php echo $value['forks_count'] ?>
                                </div>

                                <div class="item-info">
                                    <span title="Watchers"  class="glyphicon glyphicon-eye-open" aria-hidden="true"></span><?php echo $value['watchers_count'] ?>
                                </div>

                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } ?>
