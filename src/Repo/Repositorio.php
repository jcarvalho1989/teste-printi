<?php

use Github\Client;
use Github\ResultPager;

class Repositorio 
{  
    private $dados;

    public function __construct($repositorio = 'symfony') 
    {
        $client = new Client();
        $organizationApi = $client->api('organization');
        $paginator = new ResultPager($client);
        $parameters = array($repositorio);
        $this->dados = $paginator->fetchAll($organizationApi, 'repositories', $parameters);
    }
    
    
    
}
