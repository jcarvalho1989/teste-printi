<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Teste Printi</title>
        <!-- Bootstrap -->
        <link href="src/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="src/assets/css/style.css" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <div class="jumbotron">
                <h1>Teste Printi</h1>
                <p class="lead">Create a new PHP application from scratch, and using composer* add PHP GitHub API 2.0
                    as its dependency.</p>
                <p class="lead"> After that, use this library to list in a single page all the Symfony repositories hosted on
                    GitHub.</p>
                <p class="lead">
                    Please follow Symfony Coding Standards when you're coding this application
                    Create a GitHub or Bitbucket public repository to host the code.
                </p>
                <p class="lead call_to_action">

                    <strong>Clique no botão ao lado para listar os repositórios do Symfony no GitHub:  </strong>  

                    <button class="btn btn-lg btn-success pull-right" id="get_repo" type="submit"> <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                        Repositórios
                    </button>
                </p>
            </div>
            <div class="repositorios"></div>
     
                <footer class="footer">
                    <p class="text-center">&copy; 2017 Josiano Carvalho.</p>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                    <script src="src/assets/bootstrap/js/bootstrap.min.js"></script>
                    <script src="src/assets/js/init.js"></script>
                </footer>
                 </div> <!-- /container -->
    </body>
</html>

